variable "vpc_private_subnets" {
  type        = list(string)
  description = "private subnets cidr blocks "
}

variable "vpc_public_subnets" {
  type        = list(string)
  description = "public subnets cidr blocks"
}

variable "vpc_cidr_block" {
  type        = string
  description = "cidr block for vpc"
}

variable "region" {
  type        = string
  description = "region in which infra will be created"

}

variable "environment" {
  type        = string
  description = "environment name. this will be tagged to each resources created"
}
