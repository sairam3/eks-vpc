
terraform {
  required_version = ">= 0.14.5"
  required_providers {
    aws = {
      version = "3.56.0"
    }
  }
}

provider "aws" {
  region = var.region
}

