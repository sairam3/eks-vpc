variable "region" {
  type        = string
  description = "region in which infra will be created"

}

variable "environment" {
  type        = string
  description = "environment name. this will be tagged to each resources created"
}

variable "eks_config_output_path" {
  type        = string
  description = "path where kubeconfig file will be stored"
}


variable "eks_version" {
  type =string
  description="eks version"
}

variable "eks_worker_volume_type" {
  type =string
  description="eks worker volume type"
}

variable "eks_worker_asg_min_capacity" {
  type =number
  description="eks worker minimum capacity"
}

variable "eks_worker_asg_max_capacity" {
  type =number
  description="eks worker maximum capacity"
}

variable "eks_worker_asg_desired_capacity" {
  type =number
  description="eks worker desired capacity"
}

variable "eks_worker_instance_type" {
  type =string
  description="eks worker instance type"
}


variable "acceldata_repository" {
  type = string
  description = "http endpoint of acceladata chart repo"
}


variable "acceldata_repository_username" {
  type = string
  description = "username of acceladata chart repo"
  sensitive = true
}


variable "acceldata_repository_password" {
  type = string
  description = "password for  acceladata chart repo"
  sensitive = true

}

variable "acceldata_vpn_cidr" {
  type = list(string)
  description = "VPN address from only which access to public end point can be accessed"
  sensitive = false

}

variable "vpc_id" {
  type = string
  description = "aws vpc in which resource will be created"
}

variable "private_subnet_ids" {
  type = list(string)
  description = "private subnet ids"
  
}

