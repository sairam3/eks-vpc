#NOTE 1: added format so that output can  be directly fed to next stage

output "eks_cluster_id" {
  value =  module.ad_eks.eks_cluster_id
}

output "eks_cluster_endpoint" {
  value =  module.ad_eks.eks_cluster_endpoint
}


output "region" {
  value =  var.region
}

output "environment" {
  value =  var.environment
}

output "eks_cluster_name" {
  value =  local.cluster_name
  
}

output "acceldata_repository" {
  value = var.acceldata_repository
}

output "acceldata_repository_username" {
  value = var.acceldata_repository_username
  sensitive = true
}

output "acceldata_repository_password" {
  value = var.acceldata_repository_password
  sensitive = true
}

output "kms_key_id" {
    value = aws_kms_key.ad_kms.id
}

output "kms_key_arn" {
    value = aws_kms_key.ad_kms.arn
}

output "acceldata_vpn_cidr"{
  value = var.acceldata_vpn_cidr
}