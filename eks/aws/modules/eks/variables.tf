variable "region" {
  type        = string
  description = "region in which eks needs to  be create"
}

variable "eks_cluster_version" {
  type        = string
  description = "eks cluster version"

}
variable "vpc_id" {
  type        = string
  description = "vpc id"
}

variable "private_subnets" {
  type        = list(string)
  description = "list of private subnets"
}

variable "environment_name" {
  type        = string
  description = "environment name"
}

variable "eks_worker_volume_type" {
  type        = string
  description = "eks worker group ebs volume type"
}

variable "eks_worker_asg_min_capacity" {
  type        = number
  description = "minimum worker count"
}

variable "eks_worker_asg_max_capacity" {
  type        = number
  description = "max worker count"
}


variable "eks_worker_asg_desired_capacity" {
  type        = number
  description = "desired capacity"
}

variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
  ]
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [

  ]
}


variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::191579300362:user/ecs-user-k8s"
      username = "ecs-user-k8s"
      groups   = ["system:masters"]
    }
  ]
  
}


variable "eks_worker_instance_type" {
  type        = string
  description = "worker instance type"
}

variable "cluster_name" {
  type        = string
  description = "eks cluster name"
}



variable "acceldata_repository" {
  type = string
  description = "http endpoint of acceladata chart repo"
}


variable "acceldata_repository_username" {
  type = string
  description = "username of acceladata chart repo"
}


variable "acceldata_repository_password" {
  type = string
  description = "password for  acceladata chart repo"
}


variable "acceldata_vpn_cidr" {
  type = list(string)
  description = "VPN address from only which access to public end point can be accessed"
  sensitive = false

}

variable "kms_arn" {
  type = string
  description = "kms key arn"
}