awsRegion: ${region}

rbac:
  create: true
  serviceAccount:
    # This value should match local.k8s_service_account_name in locals.tf
    name: cluster-autoscaler-aws-cluster-autoscaler-chart
    annotations:
      # This value should match the ARN of the role created by module.iam_assumable_role_admin in irsa.tf
      eks.amazonaws.com/role-arn: "arn:aws:iam::${account_id}:role/cluster-autoscaler-${environment_name}"

autoDiscovery:
  clusterName: ${cluster_name}
  enabled: true