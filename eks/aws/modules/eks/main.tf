
module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version = "17.20.0"
  cluster_name    = var.cluster_name
  cluster_version = var.eks_cluster_version
  subnets         = var.private_subnets
  enable_irsa     = true

  vpc_id = var.vpc_id

  cluster_enabled_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  cluster_encryption_config = [
    {
      provider_key_arn = var.kms_arn
      resources        = ["secrets"]
    }
  ]

  cluster_endpoint_public_access_cidrs = var.acceldata_vpn_cidr
  cluster_endpoint_private_access      = true
  cluster_endpoint_public_access       = true

  node_groups = {
    workers = {
      desired_capacity = var.eks_worker_asg_desired_capacity
      max_capacity     = var.eks_worker_asg_max_capacity
      min_capacity     = var.eks_worker_asg_min_capacity

      instance_types = [var.eks_worker_instance_type]
      capacity_type  = "ON_DEMAND"
      k8s_labels = {
        GithubRepo = "terraform-aws-eks"
        GithubOrg  = "terraform-aws-modules"
      }
      additional_tags = {
        environment_name = var.environment_name
      }
      version = var.eks_cluster_version
    }
  }

  

  workers_group_defaults = {
    root_volume_type = var.eks_worker_volume_type
  }

  write_kubeconfig = false
  map_roles        = var.map_roles
  map_users        = var.map_users
  map_accounts     = var.map_accounts
  tags = {
    environment_name          = var.environment_name
  }

}

