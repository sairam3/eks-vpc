locals {
  cluster_name    = "eks-${local.environment_tag}"
  environment_tag = lower(var.environment)
}