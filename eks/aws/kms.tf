resource "aws_kms_key" "ad_kms" {

  description = "kms encryption key for vault and eks"
  tags = {
    environment_name = local.environment_tag
  }
  enable_key_rotation = true

}