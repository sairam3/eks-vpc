module "ad_eks" {
  source = "./modules/eks"

  cluster_name                = local.cluster_name
  eks_cluster_version         = var.eks_version
  eks_worker_asg_min_capacity = var.eks_worker_asg_min_capacity
  eks_worker_asg_max_capacity = var.eks_worker_asg_max_capacity

  eks_worker_asg_desired_capacity = var.eks_worker_asg_desired_capacity

  region = var.region

  eks_worker_volume_type   = var.eks_worker_volume_type
  eks_worker_instance_type = var.eks_worker_instance_type

  environment_name = local.environment_tag
  private_subnets = var.private_subnet_ids
  vpc_id          = var.vpc_id

  acceldata_repository= var.acceldata_repository
  acceldata_repository_username = var.acceldata_repository_username
  acceldata_repository_password = var.acceldata_repository_password
  acceldata_vpn_cidr = var.acceldata_vpn_cidr
  kms_arn = aws_kms_key.ad_kms.arn
}
